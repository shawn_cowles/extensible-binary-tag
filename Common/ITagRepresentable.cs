﻿namespace ExtensibleBinaryTag
{
    /// <summary>
    /// An interface defining objects that can be represented as XBTags.
    /// </summary>
    public interface ITagRepresentable
    {
        /// <summary>
        /// Produce an XBTag representing this object.
        /// </summary>
        /// <param name="tagName">The name of the resulting tag.</param>
        /// <returns></returns>
        XBTag ToTag(string tagName);
    }
}
