﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExtensibleBinaryTag
{
    /// <summary>
    /// A single tag which can contain a payload or children.
    /// </summary>
    public class XBTag : IEnumerable<XBTag>
    {
        private List<XBTag> _children;

        /// <summary>
        /// The type of this tag, type determines how the tag is written.
        /// </summary>
        public TagType Type { get; set; }

        /// <summary>
        /// The name of this tag.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The payload of this tag.
        /// </summary>
        public object Payload { get; set; }

        /// <summary>
        /// Construct a new XBTag.
        /// </summary>
        /// <param name="type">The type of the tag.</param>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(TagType type, string name, object payload)
        {
            Type = type;
            Name = name;
            Payload = payload;
            _children = new List<XBTag>();
        }

        /// <summary>
        /// Construct a new XBTag as a container for other tags.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        public XBTag(string name)
            : this(TagType.TAG_EMPTY, name, null)
        {
        }

        #region Typed Constructors
        /// <summary>
        /// Construct a new XBTag for a byte array payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, byte[] payload)
            :this(TagType.TAG_BYTE_ARRAY, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a double payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, double payload)
            : this(TagType.TAG_DOUBLE, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a GUID payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, Guid payload)
            : this(TagType.TAG_GUID, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for an int payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, int payload)
            : this(TagType.TAG_INT, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for an int array payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, int[] payload)
            : this(TagType.TAG_INT_ARRAY, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a float payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, float payload)
            : this(TagType.TAG_SINGLE, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a float array payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, float[] payload)
            : this(TagType.TAG_SINGLE_ARRAY, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a string payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, string payload)
            : this(TagType.TAG_STRING, name, payload)
        {
        }

        /// <summary>
        /// Construct a new XBTag for a bool payload.
        /// </summary>
        /// <param name="name">The name of the tag.</param>
        /// <param name="payload">The payload of the tag.</param>
        public XBTag(string name, bool payload)
            : this(TagType.TAG_BOOL, name, payload)
        {
        }
        #endregion

        /// <summary>
        /// Add a child to this tag.
        /// </summary>
        /// <param name="child">The child to add.</param>
        public void AddChild(XBTag child)
        {
            _children.Add(child);
        }

        /// <summary>
        /// Add several children to this tag.
        /// </summary>
        /// <param name="children">The child tags to add.</param>
        public void AddChildren(IEnumerable<XBTag> children)
        {
            _children.AddRange(children);
        }

        /// <summary>
        /// The number of children of this tag.
        /// </summary>
        public int ChildCount
        {
            get { return _children.Count; }
        }

        /// <summary>
        /// Retrieve a child tag from this tag by index.
        /// </summary>
        /// <param name="index">The index of the child to return.</param>
        /// <returns>The specified child.</returns>
        /// <throws>A <see cref="IndexOutOfRangeException"/> if the tag does not contain at the
        /// specified index.</throws>
        public XBTag GetChild(int index)
        {
            return _children[index];
        }
        
        /// <summary>
        /// Retrieve a child tag from this tag by name.
        /// </summary>
        /// <param name="name">Name of the child to retrieve.</param>
        /// <returns>The specified child.</returns>
        /// <throws>A <see cref="KeyNotFoundException"/> if the tag does not contain a child with
        /// the specified name.</throws>
        public XBTag GetChild(string name)
        {
            foreach (var child in _children)
            {
                if (child.Name == name)
                {
                    return child;
                }
            }

            throw new KeyNotFoundException("No child tags matching name: " + name);
        }

        /// <summary>
        /// Determine if this tag contains a child tag of the specified name.
        /// </summary>
        /// <param name="name">The name to check for.</param>
        /// <returns>True if this tag contains a child of the specified name, false otherwise.</returns>
        public bool HasChild(string name)
        {
            foreach (var child in _children)
            {
                if (child.Name == name)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// String representation of this tag.
        /// </summary>
        /// <returns>String representation of this tag.</returns>
        public override string ToString()
        {
            return "<" + Name + " : " + Type + ">";
        }

        /// <summary>
        /// Get an enumerator over the children of this tag.
        /// </summary>
        /// <returns>An enumerator over the children of this tag.</returns>
        public IEnumerator<XBTag> GetEnumerator()
        {
            return _children.GetEnumerator();
        }

        /// <summary>
        /// Get an enumerator over the children of this tag.
        /// </summary>
        /// <returns>An enumerator over the children of this tag.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _children.GetEnumerator();
        }

        /// <summary>
        /// Get the payload of this tag cast to a specified type.
        /// </summary>
        /// <typeparam name="T">The type to cast the payload to.</typeparam>
        /// <returns></returns>
        public T GetPayload<T>()
        {
            return (T)Payload;
        }

        /// <summary>
        /// Get the payload of a child, or a default value if the child does not exist.
        /// </summary>
        /// <typeparam name="T">The type to cast the payload to.</typeparam>
        /// <param name="childName">The name of the child.</param>
        /// <returns>The payload of the named child, or default(T) if the child does not exist.</returns>
        public T PayloadOfChildOrDefault<T>(string childName)
        {
            if(HasChild(childName))
            {
                return GetChild(childName).GetPayload<T>();
            }

            return default(T);
        }

        #region Collection Helpers
        /// <summary>
        /// Build a tag that contains a collection of objects as tags.
        /// </summary>
        /// <typeparam name="T">The type of object in the collection.</typeparam>
        /// <param name="containerName">The tag name for the container tag.</param>
        /// <param name="collection">The collection of objects tags to place in the container.</param>
        /// <returns>An XBTag representing <paramref name="collection"/>.</returns>
        public static XBTag BuildCollection<T>(string containerName,
            IEnumerable<T> collection) where T : ITagRepresentable
        {
            var container = new XBTag(containerName);

            container.AddChildren(collection.Select(x => x.ToTag("")));

            return container;
        }

        /// <summary>
        /// Build a tag that contains a collection of objects as tags.
        /// </summary>
        /// <typeparam name="T">The type of object in the collection.</typeparam>
        /// <param name="containerName">The tag name for the container tag.</param>
        /// <param name="collection">The collection of objects tags to place in the container.</param>
        /// <param name="writeFunction">The function to convert <typeparamref name="T"/>s to <see cref="XBTag"/>s.</param>
        /// <returns>An XBTag representing <paramref name="collection"/>.</returns>
        public static XBTag BuildCollection<T>(string containerName, IEnumerable<T> collection,
            Func<T, XBTag> writeFunction)
        {
            var container = new XBTag(containerName);

            container.AddChildren(collection.Select(x => writeFunction(x)));

            return container;
        }

        /// <summary>
        /// Read a collection of objects from a tag.
        /// </summary>
        /// <typeparam name="T">The type of object to read.</typeparam>
        /// <param name="container">The container to read the collection from.</param>
        /// <param name="readFunction">The function to convert <see cref="XBTag"/>s to <typeparamref name="T"/>s.</param>
        /// <returns>An enumerable of <typeparamref name="T"/>s produced from the children of <paramref name="container"/>.</returns>
        public static IEnumerable<T> ReadCollection<T>(XBTag container, Func<XBTag, T> readFunction)
        {
            return container.Select(x => readFunction(x));
        }

        /// <summary>
        /// Build a tag that contains a dictionary of objects.
        /// </summary>
        /// <typeparam name="K">The type of keys in the dictionary.</typeparam>
        /// <typeparam name="V">The type of values in the dictionary.</typeparam>
        /// <param name="containerName">The tag name for the container tag.</param>
        /// <param name="dictionary">The <see cref="IDictionary"/> to represent in the tag.</param>
        /// <param name="tagifyKey">A function to produce a tag for the key of each element in
        /// <paramref name="dictionary"/>.</param>
        /// <param name="tagifyValue">A function to produce a tag for the value of each element in
        /// <paramref name="dictionary"/>.</param>
        /// <returns></returns>
        public static XBTag BuildDictionary<K, V>(string containerName, Dictionary<K, V> dictionary,
            Func<K, XBTag> tagifyKey, Func<V, XBTag> tagifyValue)
        {
            var root = new XBTag(containerName);

            root.AddChildren(dictionary.Select(
                x =>
                {
                    var wrapper = new XBTag("");
                    wrapper.AddChild(tagifyKey(x.Key));
                    wrapper.AddChild(tagifyValue(x.Value));
                    return wrapper;
                }
            ));

            return root;
        }

        /// <summary>
        /// Read a dictionary from a tag.
        /// </summary>
        /// <typeparam name="K">The type of keys in the dictionary.</typeparam>
        /// <typeparam name="V">The type of values in the dictionary.</typeparam>
        /// <param name="container">The tag containing the dictionary.</param>
        /// <param name="readKey">A function to produce a key from an <see cref="XBTag"/>.</param>
        /// <param name="readValue">A function to produce a value from an <see cref="XBTag"/>.</param>
        /// <returns>A dictionary read from <paramref name="container"/>.</returns>
        public static Dictionary<K, V> ReadDictionary<K, V>(XBTag container, Func<XBTag, K> readKey,
            Func<XBTag, V> readValue)
        {
            var dictionary = new Dictionary<K, V>();

            foreach (var child in container)
            {
                var key = readKey(child.GetChild(0));
                var value = readValue(child.GetChild(1));
                dictionary.Add(key, value);
            }

            return dictionary;
        }

        /// <summary>
        /// Build a tag that contains a dictionary of strings. Stored more efficiently than 
        /// BuildDictionary.
        /// </summary>
        /// <param name="containerName">The tag name for the container tag.</param>
        /// <param name="dictionary">The dictionary of strings to place in the container.</param>
        /// <returns>An XBTag representing <paramref name="dictionary"/>.</returns>
        public static XBTag BuildStringDictionary(string containerName, IDictionary<string, string> dictionary)
        {
            var container = new XBTag(containerName);

            container.AddChildren(dictionary.Select(x => new XBTag(x.Key, x.Value)));

            return container;
        }

        /// <summary>
        /// Read a string dictionary from a tag.
        /// </summary>
        /// <param name="container"> The tag containing the dictionary.</param>
        /// <returns>A string dictionary read from <paramref name="container"/>.</returns>
        public static Dictionary<string, string> ReadStringDictionary(XBTag container)
        {
            return container.ToDictionary(t => t.Name, t => t.GetPayload<string>());
        }
        #endregion

        #region Writing
        /// <summary>
        /// Write a tag and all of its children to a stream.
        /// </summary>
        /// <param name="root">The root tag to write.</param>
        /// <param name="stream">The stream to write to.</param>
        public static void WriteToStream(XBTag root, Stream stream)
        {
            var dictionary = new Dictionary<string, short>();

            FormDictionary(root, dictionary);

            using (var output = new BinaryWriter(stream))
            {
                output.Write(dictionary.Count);

                foreach (var name in dictionary.Keys)
                {
                    var key = dictionary[name];

                    output.Write(key);

                    var chars = name.ToCharArray();

                    output.Write(chars.Length);
                    output.Write(chars);
                }

                WriteTagTree(root, output, dictionary);
            }
        }

        private static void FormDictionary(XBTag root, Dictionary<string, short> dictionary)
        {
            if (!dictionary.ContainsKey(root.Name))
            {
                dictionary.Add(root.Name, (short)dictionary.Count);
            }

            for (int i = 0; i < root.ChildCount; i++)
            {
                FormDictionary(root.GetChild(i), dictionary);
            }
        }

        private static void WriteTagTree(XBTag root, BinaryWriter output, Dictionary<string, short> dictionary)
        {
            output.Write((int)root.Type);

            output.Write(dictionary[root.Name]);

            try
            {
                switch (root.Type)
                {
                    case TagType.TAG_DOUBLE:
                        output.Write((double)root.Payload);
                        break;
                    case TagType.TAG_EMPTY:
                        break;
                    case TagType.TAG_INT:
                        output.Write((int)root.Payload);
                        break;
                    case TagType.TAG_SINGLE:
                        output.Write((float)root.Payload);
                        break;
                    case TagType.TAG_STRING:
                        var chars = ((string)root.Payload).ToCharArray();
                        output.Write(chars.Length);
                        output.Write(chars);
                        break;
                    case TagType.TAG_INT_ARRAY:
                        var arr = root.Payload as int[];
                        output.Write(arr.Length);
                        for (int i = 0; i < arr.Length; i++)
                        {
                            output.Write(arr[i]);
                        }
                        break;
                    case TagType.TAG_SINGLE_ARRAY:
                        var sarr = root.Payload as float[];
                        output.Write(sarr.Length);
                        for (int i = 0; i < sarr.Length; i++)
                        {
                            output.Write(sarr[i]);
                        }
                        break;
                    case TagType.TAG_BYTE_ARRAY:
                        var barr = root.Payload as byte[];
                        output.Write(barr.Length);
                        output.Write(barr);
                        break;
                    case TagType.TAG_GUID:
                        output.Write(((Guid)root.Payload).ToByteArray());
                        break;
                    case TagType.TAG_BOOL:
                        output.Write((bool)root.Payload);
                        break;
                    default:
                        throw new ArgumentException("Unknown tag type: " + root.Type);
                }
            }
            catch (InvalidCastException ex)
            {
                throw new InvalidCastException(ex.Message + " Tag name: " + root.Name);
            }

            output.Write(root.ChildCount);
            for (int i = 0; i < root.ChildCount; i++)
            {
                WriteTagTree(root.GetChild(i), output, dictionary);
            }
        }
        #endregion

        #region Reading
        /// <summary>
        /// Read a tag from a stream.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <returns>The XBTag and children represented by the stream.</returns>
        public static XBTag ReadFromStream(Stream stream)
        {
            XBTag result;

            using (var input = new BinaryReader(stream))
            {
                var dictSize = input.ReadInt32();

                var dictionary = new Dictionary<short, string>();

                for (int i = 0; i < dictSize; i++)
                {
                    var key = input.ReadInt16();
                    var nameLen = input.ReadInt32();
                    var nameChars = input.ReadChars(nameLen);

                    dictionary.Add(key, new string(nameChars));
                }

                result = ReadTagTree(input, dictionary);
            }

            return result;
        }

        private static XBTag ReadTagTree(BinaryReader input, Dictionary<short, string> dictionary)
        {
            var type = (TagType)input.ReadInt32();

            var nameKey = input.ReadInt16();

            var name = dictionary[nameKey];

            object payload;

            switch (type)
            {
                case TagType.TAG_DOUBLE:
                    payload = input.ReadDouble();
                    break;
                case TagType.TAG_EMPTY:
                    payload = null;
                    break;
                case TagType.TAG_INT:
                    payload = input.ReadInt32();
                    break;
                case TagType.TAG_SINGLE:
                    payload = input.ReadSingle();
                    break;
                case TagType.TAG_STRING:
                    var payLength = input.ReadInt32();
                    var payChars = input.ReadChars(payLength);
                    payload = new string(payChars);
                    break;
                case TagType.TAG_INT_ARRAY:
                    var arrLength = input.ReadInt32();
                    var arr = new int[arrLength];
                    for (int i = 0; i < arrLength; i++)
                    {
                        arr[i] = input.ReadInt32();
                    }
                    payload = arr;
                    break;
                case TagType.TAG_SINGLE_ARRAY:
                    var sarrLength = input.ReadInt32();
                    var sarr = new float[sarrLength];
                    for (int i = 0; i < sarrLength; i++)
                    {
                        sarr[i] = input.ReadSingle();
                    }
                    payload = sarr;
                    break;
                case TagType.TAG_BYTE_ARRAY:
                    var barrLength = input.ReadInt32();
                    payload = input.ReadBytes(barrLength);
                    break;
                case TagType.TAG_GUID:
                    payload = new Guid(input.ReadBytes(16));
                    break;
                case TagType.TAG_BOOL:
                    payload = input.ReadBoolean();
                    break;
                default:
                    throw new ArgumentException("Unknown tag type: " + type);
            }

            var tag = new XBTag(type, name, payload);

            int childCount = input.ReadInt32();

            for (int i = 0; i < childCount; i++)
            {
                tag.AddChild(ReadTagTree(input, dictionary));
            }

            return tag;
        }
        #endregion

    }
}
