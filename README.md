# ExtensibleBinaryTag #

Shawn Cowles, 2014

A library for reading and writing data to files in a compact format. Data is structured in nested XBTags, each of which can have a payload and/or child tags. Tags are identified by a string name and can be written to any stream in a binary format.

Tags (and all children) can be read from a stream

`var tag = XBTag.ReadFromStream(stream);`

Created with a specified type and name

`tag = new XBTag(type, name, payload);`

Or created with a type determined by the payload

`tag = new XBTag(name, stringPayload);`

And they can be easily written to a stream

`XbTag.WriteToStream(rootTag, stream);`

There are also several helper methods for writing and reading collections of tags.

--------------------------------------------

Code licensed under the MIT License. See LICENSE.txt for details.