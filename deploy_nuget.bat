call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat"

msbuild /p:Configuration=Release /p:Platform="Any CPU"

nuget pack ExtensibleBinaryTag.nuspec

nuget push *.nupkg -Source https://www.nuget.org/api/v2/package
del *.nupkg

echo "Deployment Complete"