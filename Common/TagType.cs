﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtensibleBinaryTag
{
    /// <summary>
    /// Enum for the various types of <see cref="XBTag"/>. Used to determine how to serialize the tag.
    /// </summary>
    public enum TagType
    {
        /// <summary>
        /// A tag containing a <see cref="string"/>.
        /// </summary>
        TAG_STRING,

        /// <summary>
        /// A tag containing a <see cref="double"/>.
        /// </summary>
        TAG_DOUBLE,

        /// <summary>
        /// A tag containing a <see cref="float"/>.
        /// </summary>
        TAG_SINGLE,

        /// <summary>
        /// A tag containing an <see cref="int"/>.
        /// </summary>
        TAG_INT,

        /// <summary>
        /// A tag containing child tags.
        /// </summary>
        TAG_EMPTY,

        /// <summary>
        /// A tag containing an array of <see cref="float"/>s.
        /// </summary>
        TAG_SINGLE_ARRAY,

        /// <summary>
        /// A tag containing an array of <see cref="int"/>s.
        /// </summary>
        TAG_INT_ARRAY,

        /// <summary>
        /// A tag containing an array of <see cref="byte"/>s.
        /// </summary>
        TAG_BYTE_ARRAY,

        /// <summary>
        /// A tag containing a <see cref="Guid"/>
        /// </summary>
        TAG_GUID,

        /// <summary>
        /// A tag containing a <see cref="bool"/>.
        /// </summary>
        TAG_BOOL,
    }
}
